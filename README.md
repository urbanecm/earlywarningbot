# earlywarning bot

Provides early feedback to Gerrit patches as soon as a job fails.

## Set up (application)

This application runs on Toolforge. Quibble is configured to POST data about 
failing jobs to this application. The earlywarning bot uses that data to
craft a message, and optionally a voting label, and post a comment in the 
relevant Gerrit patch.

## Set up (repository)

In repositories that want early warnings, create a `quibble.yaml` file:

```yaml
earlywarning:
  # The bot will leave a comment with a link to the failed build console URL. Recommended.
  should_comment: 1
  # The bot will leave a -1 Verified review. That's how the build will display once
  # all jobs complete, so this is recommended as well.
  should_vote: 1
```
